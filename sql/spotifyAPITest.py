#LINK TO TUTORIAL I FOLLOWED: https://www.youtube.com/watch?v=9mKAAWRheTA 
import requests, base64, json, csv
from secrets import *
#step1: get authentication token
#step2: access the api using said token

# curl -X "POST" -H "Authorization: Basic ZjM4ZjAw...WY0MzE=" -d grant_type=client_credentials https://accounts.spotify.com/api/token
def getAccessToken(clientID, clientSecret):
    authURL = "https://accounts.spotify.com/api/token"
    authHeader = {}
    authData = {}

    #base64 encode client id and secret
    message = f"{clientID}:{clientSecret}"
    messageBytes = message.encode('ascii')
    base64Bytes = base64.b64encode(messageBytes)
    base64Message = base64Bytes.decode('ascii')
    #print(base64Message)

    #sending the post request to get our authentication token
    authHeader['Authorization'] = "Basic " + base64Message
    authData['grant_type'] = "client_credentials"
    result = requests.post(authURL, headers=authHeader, data=authData)
    #print(result)

    responseObject = result.json()
    #print(json.dumps(responseObject, indent=2))

    accessToken = responseObject['access_token']
    return accessToken


def getArtistAlbums(accessToken, artistID):
    artistAlbumsEndpoint = f"https://api.spotify.com/v1/artists/{artistID}/albums"
    getHeader = {
        "Authorization": "Bearer " + accessToken
    }

    result = requests.get(artistAlbumsEndpoint, headers=getHeader)

    artistAlbumsObject = result.json()
    #print(json.dumps(artistAlbumsObject, indent=2))
    return artistAlbumsObject

# this only works for under 20 albums so probably dont use it 
def getAlbumsTracks(accessToken, albumIDs):
    albumsEndpoint = "https://api.spotify.com/v1/albums"
    getHeader = {
        "Authorization": "Bearer " + accessToken
    }
    reqData = {
        "ids": albumIDs
    }
    result = requests.get(albumsEndpoint, headers=getHeader, params=reqData)

    albumsTracksObject = result.json()
    #print(json.dumps(albumTracksObject, indent=2))
    return albumsTracksObject

#for single album use only
def getAlbumTracks(accessToken, albumID):
    albumsEndpoint = f"https://api.spotify.com/v1/albums/{albumID}"
    getHeader = {
        "Authorization": "Bearer " + accessToken
    }
    result = requests.get(albumsEndpoint, headers=getHeader)
    albumTracksObject = result.json()
    #print(json.dumps(albumTracksObject, indent=2))
    return albumTracksObject

#-----------MAIN-----------
token = getAccessToken(clientID, clientSecret)
artistID = "2pAWfrd7WFF3XhVt9GooDL" #MF DOOM: 2pAWfrd7WFF3XhVt9GooDL

artistAlbumsObject = getArtistAlbums(token, artistID)
# print(json.dumps(artistAlbumsObject, indent=2))

#build a list of the artist's album IDs
csvList = []
i = 0
for album in artistAlbumsObject['items']:
    albumID = album['id']
    tracks = getAlbumTracks(token, albumID)
    for single in tracks['tracks']['items']:
        trackName = single['name']
        artistNames = ""
        for name in single['artists']:
            artistNames += name['name'] + " & "
        csvList.append((trackName, artistNames[0:len(artistNames)-3]))
        i += 1
print("Num songs:", i)

outputCSV = 'output.csv'
fieldnames = ['title', 'artist']
with open(outputCSV, 'w') as csvFile:
    writer = csv.DictWriter(csvFile, fieldnames=fieldnames)
    writer.writeheader()
    for data in csvList:
        print({'Title': data[1]})
        writer.writerow({'title': data[0], 'artist': data[1]})
    


# tracksObject = getAlbumTracks(token, albumIDList)
# for album in tracksObject['albums']:
#     tracks = album['tracks']['items']
#     for single in tracks:
#         print(single['name'])

    