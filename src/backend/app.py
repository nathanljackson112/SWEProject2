from flask import Flask, send_from_directory
from flask_restful import Api, Resource, reqparse
# from flask_cors import CORS

app = Flask(__name__, static_url_path='/', static_folder='../frontend/build')
# CORS(app)
api = Api(app)

@app.route("/abc")
def index():
    return app.send_static_file('index.html')