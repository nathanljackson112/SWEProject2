# SWEProject2
Group 18

Andrew Li, Nathaniel Jackson, Connor Kite, Mengning Geng, Blake Romero

[MusicFinder (subject to change)]
A website for people to find artists/songs to listen to based on their interests as well as events/concerts to possibly attend

[3 separate URLS]
https://developer.spotify.com/documentation/web-api/
https://app.swaggerhub.com/apis/Bandsintown/PublicAPI/3.0.0
https://docs.genius.com/
https://lyricsovh.docs.apiary.io/#
https://developer.ticketmaster.com/products-and-docs/apis/getting-started/

[3 Models]
Songs (Genre, Danceability, Artist, Tempo, popularity(listens?))
Artists (Time period, genre, gender, number of songs/albums?, # of listens, nationality)
Location/Concerts (city, dates, genre, artist, venue)

[number of instances of each model]
spotify has 50 million songs, so up to that amount for songs
spotify has 3 million artists so up to that amount
around 100 concerts? this one can vary a lot

each model should be rich with media (embedded play, sliders to sort by, song picture, artist picture, location lookup, lyric lookup)

5 additional attributes for each model you can sear on the instance pages
Songs: Valence, Energy, Speechiness, Instrumentalness, Liveness, Acousticness, recommendations
Artist: top tracks, related artists, albums, songs, genius page, concerts?
Concert/event: time of event, address of venue, capacity of venue, tickets?, description

two types of media to display on the instance pages (artist pic, song pic, embedded play, pics of venue)

Civic engagement: help lesser known artists get listens by having part of our project for find lesser known "similar" songs

What 3 questions will we answer using this site
Can we find events/concerts for people to attend based on factors they like in music/artists?
What genres of music are the most popular online?
What genres of music are the most popular at events/concerts?

Gitlab Repo: https://gitlab.com/ckite2k/SWEProject2
